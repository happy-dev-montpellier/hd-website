# hd-website

> HappyDev Montpellier website (NUXT version)

## Build setup

### Development

```sh
npm run dev
```

### Production

```sh
npm run start
```

## Config

- Create .env with required enviromental variables (check env-example)

## Questions

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
