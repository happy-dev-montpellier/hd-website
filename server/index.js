require("dotenv").config({
  silent: true
})

const { loadNuxt, build } = require('nuxt')
const express = require('express');
const app = express()
const isDev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000

start()

async function start () {
  const nuxt = await loadNuxt(isDev ? 'dev' : 'start')

  const api = express.Router()
  api.get("*",(req,res)=>{
    const axios = require('axios')
    axios.get(process.env.STRAPI_URL+req.url).then(response=>{
      res.json(response.data);
    }).catch(err=>{
      console.log(err)
      res.json({})
    })
  })
  app.use('/api',api);

  app.use(nuxt.render)

  if (isDev) {
    build(nuxt)
  }

  app.listen(port, '0.0.0.0')

  console.log(`Server listening on localhost:${port}.`)
}
